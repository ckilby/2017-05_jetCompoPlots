from ROOT import *

class AtlasStyle(object) :

   @staticmethod
   def SetAtlasStyle(font = 42, size = 0.05) :
      if not hasattr(AtlasStyle.SetAtlasStyle, "styleset") :
         AtlasStyle.SetAtlasStyle.styleset = True
      else :
         return
      atlasStyle = TStyle("ATLAS","Atlas style")

      # use plain black on white colors
      icol = 0 # WHITE
      atlasStyle.SetFrameBorderMode(icol)
      atlasStyle.SetFrameFillColor(icol)
      atlasStyle.SetCanvasBorderMode(icol)
      atlasStyle.SetCanvasColor(icol)
      atlasStyle.SetPadBorderMode(icol)
      atlasStyle.SetPadColor(icol)
      atlasStyle.SetStatColor(icol)
      atlasStyle.SetLegendFillColor(icol)
      #atlasStyle.SetFillColor(icol) # don't use: white fill color for *all* objects

      # set the paper & margin sizes
      atlasStyle.SetPaperSize(20,26)

      # set margin sizes
      atlasStyle.SetPadTopMargin(0.05)
      atlasStyle.SetPadRightMargin(0.05)
      atlasStyle.SetPadBottomMargin(0.16)
      atlasStyle.SetPadLeftMargin(0.16)

      # set title offsets (for axis label)
      atlasStyle.SetTitleXOffset(1.4)
      atlasStyle.SetTitleYOffset(1.4)

      # use large fonts
      #Int_t font=72 # Helvetica italics
      # font = 42 # Helvetica
      # Double_t size = 0.05
      atlasStyle.SetTextFont(font)
      atlasStyle.SetLabelFont(font,"x")
      atlasStyle.SetTitleFont(font,"x")
      atlasStyle.SetLabelFont(font,"y")
      atlasStyle.SetTitleFont(font,"y")
      atlasStyle.SetLabelFont(font,"z")
      atlasStyle.SetTitleFont(font,"z")
      atlasStyle.SetLegendFont(font)

      atlasStyle.SetTextSize(size)
      atlasStyle.SetLabelSize(size,"x")
      atlasStyle.SetTitleSize(size,"x")
      atlasStyle.SetLabelSize(size,"y")
      atlasStyle.SetTitleSize(size,"y")
      atlasStyle.SetLabelSize(size,"z")
      atlasStyle.SetTitleSize(size,"z")

      # use bold lines and markers
      atlasStyle.SetMarkerStyle(20)
      atlasStyle.SetMarkerSize(1.2)
      atlasStyle.SetHistLineWidth(2)
      #atlasStyle.SetLineStyleString(2,"[12 12]") # postscript dashes

      # get rid of X error bars
      #atlasStyle.SetErrorX(0.001)
      # get rid of error bar caps
      atlasStyle.SetEndErrorSize(0.)

      # do not display any of the standard histogram decorations
      atlasStyle.SetOptTitle(0)
      #atlasStyle.SetOptStat(1111)
      atlasStyle.SetOptStat(0)
      #atlasStyle.SetOptFit(1111)
      atlasStyle.SetOptFit(0)

      # put tick marks on top and RHS of plots
      atlasStyle.SetPadTickX(1)
      atlasStyle.SetPadTickY(1)

      # Legend setting
      atlasStyle.SetLegendBorderSize(0)
      gROOT.SetStyle("ATLAS")


