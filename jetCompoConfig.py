pathToFiles = '/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.30_newerOffline/ttHScripts/MayProd_newOff_v3_withVars/ttH-offline_Output_merge/ge2jge2b/'
dictOfFilePaths = {
    'ttH_aMcP8_FS':pathToFiles+'ttH_aMcP8_FS.root',
    
    'tt_PP8_dil_FS':pathToFiles+'ttbar_PP8_dil_FS.root',
    'tt_PP8_dil_bfil_FS':pathToFiles+'ttbar_PP8_dil_bfil_FS.root',
    'tt_PP8_lep_FS':pathToFiles+'ttbar_PP8_lep_fakes_FS.root',
    'tt_PP8_lep_bfil_FS':pathToFiles+'ttbar_PP8_lep_bfil_fakes_FS.root',
    
    'ttW_FS':pathToFiles+'ttW_FS.root',
    'ttZ_FS':pathToFiles+'ttZ_FS.root',
    
    'zjets_FS':pathToFiles+'zjets_FS.root',
    'wjets_FS':pathToFiles+'wjets_FS.root',
    'tHjb_FS':pathToFiles+'tHjb_MadgraphP8_FS.root',
    'WtH_FS':pathToFiles+'tWH_aMcHpp_FS.root',
    'tV_FS':pathToFiles+'tV_FS.root',
    'diboson_FS':pathToFiles+'diboson_FS.root',
    '4top_FS':pathToFiles+'4top_FS.root',
    'ttWW_FS':pathToFiles+'tWW_FS.root',
    'tWZ_FS':pathToFiles+'tWZ_FS.root',
    'Wtprompt_FS':pathToFiles+'Wtprompt_PP_FS.root',
    
    'Wtfakes_FS':pathToFiles+'Wtfakes_PP_FS.root',
    'schan_FS':pathToFiles+'schan_FS.root',
    'tchan_FS':pathToFiles+'tchan_FS.root',
    
    #'tt_PP6':'/scratch3/bsowden/ProductionOutput/DS2/tt_PowhegPythia6.root',
    #'ttH_aMcAtNlo_P8':'/scratch3/bsowden/ProductionOutput/DS2/ttH_aMcAtNloPythia8.root',
    #'ttV_aMcAtNlo_P8':'../../jetCompoFiles/ttV_DS2.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_aMcAtNloHpp_1l_ICHEP.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_aMcAtNloHpp_2l_ICHEP.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_aMcAtNloHpp_2l_JulyProd.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_aMcAtNloHpp_1l_2-03-49-24.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_aMcAtNloHpp_2l_2-03-49-24.root',
    #'tt_aMcAtNlo_Hpp':pathToFiles+'/ttbar_Sherpa_2l.root',
    #'tt_aMcAtNlo_Hpp':'/scratch3/bsowden/ProductionOutput/ClusterOut-May/tt_aMcAtNloPythia8_NonAllHad.root',
}

nominalTreeName = 'nominal_Loose'

#/// Enter any code needed to calculate your final normalisation here ////
tempSumWeights = 0
# tempFile = ROOT.TFile.Open(dictOfFilePaths['tt_PP6'])
# tempHist = ROOT.TH1D('h_tempIntegral', 'tempIntegral', 22, -2, 20) # Range picked in case of -1 bin and to cover whole nJets tail. Does this make sense to do?

# # The below assumes nJets is available. Any once-entry-per-event variable could supposedly be used.
# tempFile.Get(nominalTreeName).Draw('nJets >> h_tempIntegral', '(nJets >= 2 && nBTags >= 2)*(ee_2015 || ee_2016 || mumu_2015 || mumu_2016 || emu_2015 || emu_2016)*weight_mc*weight_leptonSF*weight_bTagSF_70*weight_pileup*weight_jvt', 'goff')
# tempSumWeights = tempHist.Integral()

# tempHist.Delete()
# tempFile.Close()
#/////////////////////////////////////////////////////////////////////////
renormaliseFiles  = False
renormaliseWeight = tempSumWeights
#histElement_forAutoNorm = 'Nominal'
renormaliseHistsIntegral = 1

listOfHistParams = [
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel', selection = '', isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_3jet', selection = '(nJets == 3)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(3jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_4jet', selection = '(nJets == 4)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(4jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_5jet', selection = '(nJets == 5)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(5jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_6jet', selection = '(nJets == 6)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(6jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_7jet', selection = '(nJets == 7)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(7jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_8jet', selection = '(nJets == 8)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(8jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_9jet', selection = '(nJets == 9)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(9jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    HistParams(variable = 'jet_truthPartonLabel', name = 'jet_truthPartonLabel_10jet', selection = '(nJets == 10)',  isMeV = False,  title = '', xtitle = 'jet_truthPartonLabel(10jets)', ytitle = 'Entries', nbins = 25, x_lo = 0,    x_hi = 25, logx = False,  ratio_y_lo = 0.5, ratio_y_hi = 3.),
    
    # HistParams(variable = 'HT_jets',  isMeV = True,  title = '', xtitle = 'HT_{had} (GeV)', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 2500, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'HT_all',  isMeV = True,  title = '', xtitle = 'HT_{all} (GeV)', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 2500, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'nJets',   isMeV = False, title = '', xtitle = 'nJets',          ytitle = 'Entries / bin width', nbins = 13, x_lo = 2,    x_hi = 15,   logx = False,  ratio_y_lo = 0.975,  ratio_y_hi = 1.02),
    # HistParams(variable = 'nBTags',  isMeV = False, title = '', xtitle = 'nBTags',         ytitle = 'Entries / bin width', nbins = 7,  x_lo = 2,    x_hi = 9,    logx = False,  ratio_y_lo = 0.975,  ratio_y_hi = 1.02),
    # HistParams(variable = 'Centrality_all', isMeV = False, title = '', xtitle = 'Centrality_{all}', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0, x_hi = 1, logx = False, ratio_y_lo = 0.95, ratio_y_hi = 1.075),
    # HistParams(variable = 'el_pt',   isMeV = True,  title = '', xtitle = 'Electron p_{T}', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'el_eta',  isMeV = False, title = '', xtitle = 'Electron #eta',  ytitle = 'Entries / bin width', nbins = 50, x_lo = -2.8, x_hi = 2.8,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'mu_pt',   isMeV = True,  title = '', xtitle = 'Muon p_{T}',     ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'mu_eta',  isMeV = False, title = '', xtitle = 'Muon #eta',      ytitle = 'Entries / bin width', nbins = 50, x_lo = -2.8, x_hi = 2.8,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'jet_pt',  isMeV = True,  title = '', xtitle = 'Jet p_{T}',      ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 1000, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'jet_eta', isMeV = False, title = '', xtitle = 'Jet #eta',       ytitle = 'Entries / bin width', nbins = 50, x_lo = -2.8, x_hi = 2.8,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'pTll',    isMeV = True,  title = '', xtitle = 'p_{T,ll}',       ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500,  logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),

    # HistParams(variable = 'truth_ttbar_pt', isMeV = True, title = '', xtitle = 't#bar{t} p_{T}', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'truth_top_pt',   isMeV = True, title = '', xtitle = 'top p_{T}',      ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
    # HistParams(variable = 'truth_tbar_pt',  isMeV = True, title = '', xtitle = 'anti-top p_{T}', ytitle = 'Entries / bin width', nbins = 50, x_lo = 0,    x_hi = 500, logx = False,  ratio_y_lo = 0.975, ratio_y_hi = 1.02),
]

dictOfTotSW = {}
# dictOfTotSW['t#bar{t}'] = totalSumWeights_inFile(dictOfFilePaths['tt_PP6'], 'sumWeights', 'totalEventsWeighted')
# dictOfTotSW['t#bar{t}H'] = totalSumWeights_inFile(dictOfFilePaths['ttH_aMcAtNlo_P8'], 'sumWeightsRHUL', 'totalEventsWeighted')
#dictOfTotSW['Nominal'] = totalSumWeights_inFile(dictOfFilePaths['tt_aMcAtNlo_Hpp'], 'sumWeights', 'totalEventsWeighted')
#dictOfTotSW['PDF4LHC15_nlo_30_Nom'] = totalSumWeights_inFile(dictOfFilePaths['tt_aMcAtNlo_Hpp'], 'PDFsumWeights', 'PDF4LHC15_nlo_30[0]')

# Selection strings for HF splitting
ttlightSelection = '(HF_SimpleClassification == 0 && ((mcChannelNumber == 410503 && TopHeavyFlavorFilterFlag != 5) || (mcChannelNumber == 410505 && TopHeavyFlavorFilterFlag == 5)))'
ttccSelection    = '(HF_SimpleClassification == -1 && ((mcChannelNumber == 410503 && TopHeavyFlavorFilterFlag != 5) || (mcChannelNumber == 410505 && TopHeavyFlavorFilterFlag == 5)))'
ttbbSelection    = '(HF_SimpleClassification == 1 && ((mcChannelNumber == 410503 && TopHeavyFlavorFilterFlag != 5) || (mcChannelNumber == 410505 && TopHeavyFlavorFilterFlag == 5)))'

ttFakeSelection  = '((mcChannelNumber == 410501 && TopHeavyFlavorFilterFlag != 5) || (mcChannelNumber == 410504 && TopHeavyFlavorFilterFlag == 5) || (mcChannelNumber != 410501 && mcChannelNumber != 410504))'

weightsString   = '(weight_ttbar_FracRw*weight_ttbb_Norm*weight_ttbb_Shape_SherpaNominal*weight_dilepFakesNorm*weight_mc*weight_pileup*weight_leptonSF*weight_bTagSF_Continuous*weight_jvt)'
baseSelection   = '(!nTaus)'
promptSelection = '!MCFakes_hasFakeLepton'
fakeSelection   = 'MCFakes_hasFakeLepton'
#weightsString = 'weight_mc*weight_leptonSF*weight_bTagSF_70*weight_pileup*weight_jvt'
dictOfHistElementParams = OrderedDict()
# dictOfHistElementParams['t#bar{t}'] = HistElementParams(sample = 'tt_PP6', weight = weightsString, stack_hist_list='', stack_mode='', line_color = ROOT.kBlack, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)
dictOfHistElementParams['t#bar{t}H'] = HistElementParams(sample = 'ttH_aMcP8_FS', weight = '*'.join(filter(None, [weightsString, baseSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kRed, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)

dictOfHistElementParams['t#bar{t}+light'] = HistElementParams(sample = ['tt_PP8_dil_FS', 'tt_PP8_dil_bfil_FS'], weight = '*'.join(filter(None, [weightsString, baseSelection, promptSelection, ttlightSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kCyan+1, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)
dictOfHistElementParams['t#bar{t}+#geq1c'] = HistElementParams(sample = ['tt_PP8_dil_FS', 'tt_PP8_dil_bfil_FS'], weight = '*'.join(filter(None, [weightsString, baseSelection, promptSelection, ttccSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kMagenta+1, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)
dictOfHistElementParams['t#bar{t}+#geq1b'] = HistElementParams(sample = ['tt_PP8_dil_FS', 'tt_PP8_dil_bfil_FS'], weight = '*'.join(filter(None, [weightsString, baseSelection, promptSelection, ttbbSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kBlue, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)

dictOfHistElementParams['t#bar{t}V'] = HistElementParams(sample = ['ttW_FS', 'ttZ_FS',], weight = '*'.join(filter(None, [weightsString, baseSelection, promptSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kGreen+2, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)

dictOfHistElementParams['other_{prompt}'] = HistElementParams(sample = ['zjets_FS', 'wjets_FS', 'tHjb_FS', 'WtH_FS', 'tV_FS', 'diboson_FS', '4top_FS', 'ttWW_FS', 'tWZ_FS', 'Wtprompt_FS'], weight = '*'.join(filter(None, [weightsString, baseSelection, promptSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kOrange-3, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)
dictOfHistElementParams['other_{fake}'] = HistElementParams(sample = ['tt_PP8_lep_FS', 'tt_PP8_lep_bfil_FS', 'ttW_FS', 'ttZ_FS', 'zjets_FS', 'wjets_FS', 'tHjb_FS', 'WtH_FS', 'tV_FS', 'diboson_FS', '4top_FS', 'ttWW_FS', 'tWZ_FS', 'Wtfakes_FS', 'schan_FS', 'tchan_FS'], weight = '*'.join(filter(None, [weightsString, baseSelection, fakeSelection, ttFakeSelection])), stack_hist_list='', stack_mode='', line_color = ROOT.kOrange, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)

#+'*'+str(dictOfTotSW['t#bar{t}']/dictOfTotSW['t#bar{t}H'])
#dictOfHistElementParams['Nominal'] = HistElementParams(sample = 'tt_aMcAtNlo_Hpp', weight = weightsString, stack_hist_list='', stack_mode='', line_color = ROOT.kBlack, line_style = 1, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)
#dictOfHistElementParams['PDF4LHC15_nlo_30_Nom'] = HistElementParams(sample = 'tt_aMcAtNlo_Hpp', weight = nominalTreeName+'.PDF4LHC15_nlo_30[0]*'+weightsString+'*'+str(dictOfTotSW['Nominal']/dictOfTotSW['PDF4LHC15_nlo_30_Nom']), stack_hist_list='', stack_mode='', line_color = ROOT.kBlack, line_style = 2, fill_color = ROOT.kWhite, fill_style = 0, draw_hist = True)

ratioDenom = 't#bar{t}H'
#ratioDenom = 't#bar{t}+light'
#ratioDenom = 't#bar{t}'
#ratioDenom = 'PDF4LHC15_nlo_30_Nom'

listOfAnaRegs = [
    '(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))',
    '(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)',
    '(nJets >= 4 && DilepRegion_A3 == 5)',
    '(nJets >= 4 && DilepRegion_A3 == 4)',
    '(nJets >= 4 && DilepRegion_A3 == 2)',
    '(nJets >= 4 && DilepRegion_A3 == 3)',
    '(nJets >= 4 && DilepRegion_A3 == 1)',
]

listOfChannels = [
    #'(ee || mumu || emu)'
    '(ee_2015 || ee_2016 || mumu_2015 || mumu_2016 || emu_2015 || emu_2016)'
    #'(ejets || mujets)'
    #'(ejets_2015 || ejets_2016 || mujets_2015 || mujets_2016)'
]

printFileName = False # If True, prints name of the sample file in dictOfHistElementParams as a decoration on plots
