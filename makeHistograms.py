#!/usr/bin/env python

from AtlasStyle import AtlasStyle
import ROOT
import sys
import re
import getopt
import math
from collections import namedtuple
from collections import OrderedDict

import ctypes



HistParams = namedtuple('HistParams', 'variable, name, selection, isMeV, title, xtitle, ytitle, nbins, x_lo, x_hi, logx, ratio_y_lo, ratio_y_hi')
HistElementParams = namedtuple('HistElementParams', 'sample, weight, stack_hist_list, stack_mode, draw_hist, line_color, line_style, fill_color, fill_style')
StackWithHists = namedtuple('StackWithHists', 'stack, listOfHists')

dictOfHistElementSumWeights = OrderedDict()



def totalSumWeights_inFile(filePath, treeName, varName) :
    """Utility function to return the sum over events of a totalEventsWeighted-like variable. Allows calculation of sumWeights for a sample in place e.g. allowing this to be done when defining a weights string for a TTree.Draw()"""
    tempFile = ROOT.TFile.Open(filePath)
    sumWeights = 0
    varIsVector = False
    vectorPos = 0

    if '[' in varName : # Checking if variable is a vector, formatting appropriately
        varIsVector = True
        varNameSplit = varName.split('[')
        varName = varNameSplit[0]
        varNameSplit = varNameSplit[1].split(']')
        vectorPos = varNameSplit[0]
    
    for event in tempFile.Get(treeName) :
        if varIsVector :
            sumWeights += getattr(event, varName)[int(vectorPos)]
        else :
            sumWeights += getattr(event, varName)

    tempFile.Close()    
    return sumWeights



def assertVariableExists(varName) :
    """Small utility function to check if a python variable named varName is defined when this function is called, printing an error and raising a NameError with value of varName if not"""
    
    if not varName in globals() :
        print "ERROR: "+varName+" has not been defined"
        raise NameError(varName)


    
def checkExpectedVariables() :
    """Function that checks whether the variables necessary for the histogramming have been defined (therefore also serves as the place where the list of necessary variables is defined).
    Returns False if any variables are not defined, otherwise returns True."""

    try :
        assertVariableExists('pathToFiles')
        assertVariableExists('dictOfFilePaths')
        assertVariableExists('nominalTreeName')
        assertVariableExists('renormaliseFiles')
        if renormaliseFiles is True and 'histElement_forAutoNorm' not in globals():
            assertVariableExists('renormaliseWeight')
        assertVariableExists('listOfHistParams')
        assertVariableExists('dictOfHistElementParams')
        assertVariableExists('ratioDenom')
        assertVariableExists('listOfAnaRegs')
        assertVariableExists('listOfChannels')
        assertVariableExists('printFileName')
    except NameError :
        return False

    return True

def renameHFSelStrings(selString) :    
    if 'ttlightSelection' in globals() and ttlightSelection in selString:
        selString = selString.replace(ttlightSelection, "tt+light")
    elif 'ttccSelection' in globals() and ttccSelection in selString:
        selString = selString.replace(ttccSelection, "tt+cc")
    elif 'ttbbSelection' in globals() and ttbbSelection in selString:
        selString = selString.replace(ttbbSelection, "tt+bb")

    return selString



def extremaHistOfStack(stack, extremaType) :
    """Function that operates on a stack of identically binned histograms to return a histogram filled with the highest or lowest value in each bin of the stack"""

    listOfHists = stack.GetHists()
    nBins = listOfHists.At(0).GetNbinsX()
    xLo   = listOfHists.At(0).GetXaxis().GetXmin()
    xHi   = listOfHists.At(0).GetXaxis().GetXmax()
    for hist in listOfHists : # I could avoid double checking the hist at position 0 if I knew how to slice a TList
        if hist.GetNbinsX() != nBins :
            print "WARNING: In extremaHistOfStack: number of bins are not identical for all histograms in stack. Returning an empty histogram."
            return ROOT.TH1D()       
        if hist.GetXaxis().GetXmin() != xLo :
            print "WARNING: In extremaHistOfStack: minimum x value is not identical for all histograms in stack. Returning an empty histogram."
            return ROOT.TH1D()
        if hist.GetXaxis().GetXmax() != xHi :
            print "WARNING: In extremaHistOfStack: maximum x value is not identical for all histograms in stack. Returning an empty histogram."
            return ROOT.TH1D()

    returnHist = ROOT.TH1D(stack.GetName()+' '+extremaType, stack.GetTitle()+' '+extremaType, nBins, xLo, xHi)

    for binItr in xrange(1, nBins) :
        extremaValue = listOfHists.At(0).GetBinContent(binItr)
        for hist in listOfHists :
            if extremaType in ('min', 'Min') :
                if hist.GetBinContent(binItr) < extremaValue :
                    extremaValue = hist.GetBinContent(binItr)
            elif extremaType in ('max', 'Max') :
                if hist.GetBinContent(binItr) > extremaValue :
                    extremaValue = hist.GetBinContent(binItr)
            else :
                print "WARNING: In extremaHistOfStack: an unknown extremaType has been specified. Returning an empty histogram."
                return ROOT.TH1D()

        returnHist.SetBinContent(binItr, extremaValue)

    return returnHist



def gOvergPlusl_andError(hist) :
    light = 0
    lightError = ROOT.Double(0)
    light = hist.IntegralAndError(1, 3, lightError)
    
    gluons = 0
    gluonsError = ROOT.Double(0)
    gluons = hist.IntegralAndError(21, 22, gluonsError) # gluons are 21, but using 21 to 22 just in case IntegralAndError doesn't like having a range over the same number

    if gluons == 0 :
        gOvergPlusl = 0
        gOvergPluslError = 0
    else :
        gOvergPlusl = gluons / (gluons + light)
        light_relErr = (math.pow(lightError/light, 2) if light != 0 else 0)
        gluons_relErr = math.pow(gluonsError/gluons, 2)
        
        lOvergError = math.sqrt(light_relErr + gluons_relErr)*(light/gluons)
        gOvergPluslError = lOvergError # using g/(g+l) = 1/(1+(l/g)) to make uncert calculation simpler

    return gOvergPlusl, gOvergPluslError
    

if __name__ == '__main__' :    
    usageString = 'makeHistograms.py (-h) (-o --outDir <outputDir>) <configFile>'
    outDir = 'Plots'

    try:
      opts, args = getopt.getopt(sys.argv[1:],"ho:",["outDir="])
    except getopt.GetoptError:
      print usageString
      sys.exit(2)
    for opt, arg in opts :
      if opt == '-h' :
         print usageString
         sys.exit()
      elif opt in ("-o", "--outDir") :
         outDir = arg
      else :
          print "ERROR: Unhandled option"
          print "Exiting..."
          sys.exit(1)

    print "Starting script"

    configFile_path = args[0]    
    configFile = open(configFile_path, 'r')
    
    configFile_codeObj = compile(configFile.read(), configFile_path, 'exec')
    exec(configFile_codeObj)
    if not checkExpectedVariables() :
        print "Exiting..."
        sys.exit(1)

    AtlasStyle.SetAtlasStyle()

    # CK TEMP: quick file and map for q/g
    gOvergPluslFile = open('gOvergPlusl.dat', 'w')
    gOvergPluslDict = {}
    gOvergPluslDict_vals = {}
    gOvergPluslDict_errs = {}
    gOvergPlusl_nJets_Dict = {}
    for histElement in dictOfHistElementParams :
        gOvergPluslDict[histElement] = {}
        gOvergPluslDict_vals[histElement] = {}
        gOvergPluslDict_errs[histElement] = {}
        gOvergPlusl_nJets_Dict[histElement] = {}
        for anaReg in listOfAnaRegs :
            gOvergPlusl_nJets_Dict[histElement][anaReg] = [3,4,5,6,7,8,9,10] # just filling with nJets labels to give correct length

    print "Opening root files to be read from"
    
    # Open files
    dictOfOpenFiles = {}
    for fileKey in dictOfFilePaths :
        dictOfOpenFiles[fileKey] = ROOT.TFile.Open(dictOfFilePaths[fileKey])

    print "Rearranging dictOfHistElementParams so that any stack elements are created after histogram elements"

    # Rearrange dictOfHistElementParams so that any stacks are at the end of the dict.
    # This means that all histograms are made first, so they exist by the time it comes to fill the stacks.
    nHistElements = len(dictOfHistElementParams)
    for eleCount, histElement in enumerate(dictOfHistElementParams) :
        if dictOfHistElementParams[histElement].stack_hist_list != '' :
            tempHistElement = dictOfHistElementParams[histElement]
            del dictOfHistElementParams[histElement]
            dictOfHistElementParams[histElement] = tempHistElement

        if eleCount == (nHistElements-1) : break # to avoid an infinite loop due to constantly adding stacks to the end of the dict

    print "Starting loop over analysis regions and channels"
        
    # Perform histogram creation, filling, and saving per analysis region and channel (wasteful to keep recreating hists and canvases?)
    for anaReg in listOfAnaRegs :
        for channel in listOfChannels :
            
            if renormaliseFiles :
                for histElement in dictOfHistElementParams :
                    histElementParams = dictOfHistElementParams[histElement]
                    if histElementParams.stack_hist_list == '' : # Checking this histElement is a histogram and not a stack
                        tempHist = ROOT.TH1D('h_tempIntegral', 'tempIntegral', 22, -2, 20) # Range picked in case of -1 bin and to cover whole nJets tail. Does this make sense to do?

                        # The below assumes nJets is available. Any once-entry-per-event variable could supposedly be used.
                        tempIntegral = 0.
                        if (isinstance(histElementParams.sample, list)) :
                            for subsample in enumerate(histElementParams.sample) :
                                dictOfOpenFiles[histElementParams.sample[subsample[0]]].Get(nominalTreeName).Draw('nJets >> h_tempIntegral', anaReg+'*'+channel+'*'+histElementParams.weight, 'goff')
                                tempIntegral += tempHist.Integral()
                        else :
                            dictOfOpenFiles[histElementParams.sample].Get(nominalTreeName).Draw('nJets >> h_tempIntegral', anaReg+'*'+channel+'*'+histElementParams.weight, 'goff')
                            tempIntegral = tempHist.Integral()
                            
                        dictOfHistElementSumWeights[histElement] = tempIntegral

                        if 'histElement_forAutoNorm' in globals() :
                            if histElement_forAutoNorm is histElement :
                                renormaliseWeight = tempIntegral # What if histElement_forAutoNorm == a histElements that doesn't exist?

                        tempHist.Delete()
            
            # Make hist stacks and hists
            listOfStackWithHists = []
            listOfRatioStackWithHists = []
            for params in listOfHistParams :
                tempStack = ROOT.THStack('hstack_'+params.variable, params.title)
                tempRatioStack = ROOT.THStack('hstack_ratio_'+params.variable, params.title)

                # Make and fill histograms
                listOfHists = []
                ratioDenomHist = ROOT.TH1D()
                listOfRatioHists = []
                for histElement in dictOfHistElementParams :
                    histElementParams = dictOfHistElementParams[histElement]
                    name = params.name + '_' + histElement #params.variable + '_' + histElement
                    tempHist = None
                    
                    if histElementParams.stack_hist_list == '' :
                        tempHist = ROOT.TH1D('h_'+name, name, params.nbins, params.x_lo, params.x_hi)
                        tempHist.Sumw2()

                        tempHist_subsample = tempHist.Clone("h_tempHist_subsample")

                        # If needed, renormalising files
                        if renormaliseFiles : # Put all of this in a function to tidy away the possibly numerous lookups, while also blackboxing this allowing me to refactorise to e.g. intelligently combine multiple weights
                            if dictOfHistElementSumWeights[histElement] != 0 :
                                ratio = renormaliseWeight  / dictOfHistElementSumWeights[histElement]
                            else :
                                ratio = 0
                            histElementParams = histElementParams._replace(weight = histElementParams.weight+'*'+str(ratio))
                        
                        mevRescaleWeight = 0
                        if params.isMeV :
                            mevRescaleWeight = 0.001
                        else :
                            mevRescaleWeight = 1.
                        
                        # Filling histogram
                        if (isinstance(histElementParams.sample, list)) :
                            for subsample in enumerate(histElementParams.sample) :
                                if subsample[0] == 0 :
                                    dictOfOpenFiles[histElementParams.sample[subsample[0]]].Get(nominalTreeName).Draw(params.variable+'*'+str(mevRescaleWeight)+' >> '+tempHist.GetName(), '*'.join(filter(None, [anaReg, channel, histElementParams.weight, params.selection])), 'goff')
                                else :
                                    dictOfOpenFiles[histElementParams.sample[subsample[0]]].Get(nominalTreeName).Draw(params.variable+'*'+str(mevRescaleWeight)+' >> '+tempHist_subsample.GetName(), '*'.join(filter(None, [anaReg, channel, histElementParams.weight, params.selection])), 'goff')
                                    tempHist.Add(tempHist_subsample)
                        else :
                            dictOfOpenFiles[histElementParams.sample].Get(nominalTreeName).Draw(params.variable+'*'+str(mevRescaleWeight)+' >> '+tempHist.GetName(), '*'.join(filter(None, [anaReg, channel, histElementParams.weight, params.selection])), 'goff')

                        # If needed, renormalising histogram to a fixed area
                        if 'renormaliseHistsIntegral' in globals() :
                            tempIntegral = tempHist.Integral()
                            if tempIntegral != 0 :
                                tempHist.Scale(renormaliseHistsIntegral/tempIntegral)

                        # CK TEMP: quick integrating for q/g numbers
                        gOvergPlusl, gOvergPluslError = gOvergPlusl_andError(tempHist)

                        if(params.selection.find('nJets') == -1) :
                            gOvergPluslDict[histElement][anaReg] = str("{0:.2f}".format(gOvergPlusl))+'$\pm$'+str("{0:.2f}".format(gOvergPluslError))
                            gOvergPluslDict_vals[histElement][anaReg] = gOvergPlusl
                            gOvergPluslDict_errs[histElement][anaReg] = gOvergPluslError
                        if(params.selection.find('nJets == 3') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][0] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 4') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][1] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 5') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][2] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 6') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][3] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 7') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][4] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 8') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][5] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 9') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][6] = [gOvergPlusl, gOvergPluslError]
                        if(params.selection.find('nJets == 10') != -1) :
                            gOvergPlusl_nJets_Dict[histElement][anaReg][7] = [gOvergPlusl, gOvergPluslError]
                        
                        # light = 0
                        # lightError = ROOT.Double(0)
                        # light = tempHist.IntegralAndError(1, 3, lightError)

                        # gluons = 0
                        # gluonsError = ROOT.Double(0)
                        # gluons = tempHist.IntegralAndError(21, 22, gluonsError) # gluons are 21, but using 21 to 22 just in case IntegralAndError doesn't like having a range over the same number

                        # if gluons == 0 :
                        #     gOvergPlusl = 0
                        #     gOvergPluslError = 0
                        # else :
                        #     gOvergPlusl = gluons / (gluons + light)
                        #     lOvergError = math.sqrt(math.pow(lightError/light, 2) + math.pow(gluonsError/gluons, 2))*(light/gluons)
                        #     gOvergPluslError = lOvergError # using g/(g+l) = 1/(1+(l/g)) to make uncert calculation simpler

                        # gOvergPluslDict[histElement][anaReg] = str("{0:.2f}".format(gOvergPlusl))+'$\pm$'+str("{0:.2f}".format(gOvergPluslError))
                        # #gOvergPluslFile.write(anaReg+','+name+','+str(quarks)+','+str(quarksError)+','+str(gluons)+','+str(gluonsError)+','+str(gOvergPlusl)+','+str(gOvergPluslError)+'\n')
                    else :
                        tempSubStack = ROOT.THStack('hstack_'+name, name)
                        for histStackElement in histElementParams.stack_hist_list :
                            nameStackElement = params.name + '_' + histStackElement #params.variable + '_' + histStackElement
                            tempStackElement = ROOT.gDirectory.GetList().FindObject('h_'+nameStackElement)
                            if tempStackElement :
                                tempSubStack.Add(tempStackElement)
                                
                        if histElementParams.stack_mode in ('sum', 'Sum', '') : # defaulting to sum if no stack_mode given
                            tempHist = tempSubStack.GetStack().Last()
                        elif histElementParams.stack_mode in ('min', 'Min', 'max', 'Max') :
                            tempHist = extremaHistOfStack(tempSubStack, histElementParams.stack_mode)
                        else :
                            print "WARNING: In main: unknown stack_mode given for histElemet"+histElement+". Using an empty histogram."
                            tempHist = TH1D()

                    tempHist.SetLineColor(histElementParams.line_color)
                    tempHist.SetLineStyle(histElementParams.line_style)
                    #tempHist.SetLineWidth(3)
                    tempHist.SetMarkerStyle(1)
                    tempHist.SetFillColor(histElementParams.fill_color)
                    tempHist.SetFillStyle(histElementParams.fill_style)

                    if histElement == ratioDenom :
                        ratioDenomHist = ROOT.TH1D(tempHist)

                    if histElementParams.draw_hist == True :
                        tempStack.Add(tempHist)
                    listOfHists.append(tempHist) # This is possibly dangerous/nonsensical. You end up with a listOfStackWithHists with more histograms in the list than there are in the stack
                                                 # Necessary to ensure that the histograms that aren't drawn do not get deleted

                for tempHist, histElement in zip(listOfHists, dictOfHistElementParams) :
                    histElementParams = dictOfHistElementParams[histElement]
                    tempRatioHist = ROOT.TH1D(tempHist)
                    tempRatioHist.Divide(tempRatioHist, ratioDenomHist)

                    if histElementParams.draw_hist == True :
                        tempRatioStack.Add(tempRatioHist)
                    listOfRatioHists.append(tempRatioHist)

                listOfStackWithHists.append(StackWithHists(tempStack, listOfHists))
                listOfRatioStackWithHists.append(StackWithHists(tempRatioStack, listOfRatioHists))
                
                
            # Make canvases and pads for drawing
            canvas = ROOT.TCanvas('canvas', 'Comparison histograms', 1000, 1000)
            histPad = ROOT.TPad('histPad', 'Histogram pad', 0., 0.2, 1., 1.)
            ratioPad = ROOT.TPad('ratioPad', 'Ratio pad', 0., 0., 1., 0.329)
            ratioPad.SetTopMargin(0.)
            ratioPad.SetBottomMargin(0.25)

            histPad.Draw()
            ratioPad.Draw()
            
            for stackAndHists, ratioStackAndHists, params in zip(listOfStackWithHists, listOfRatioStackWithHists, listOfHistParams) :
                    histPad.cd()
                    stackAndHists.stack.Draw('nostack E')
                    stackAndHists.stack.GetXaxis().SetTitle(params.xtitle)
                    stackAndHists.stack.GetYaxis().SetTitle(params.ytitle)
                    stackAndHists.stack.GetYaxis().SetTitleOffset(1.65)

                    legend = ROOT.TLegend(0.6,0.65,0.95,0.949)
                    for hist, histElement in zip(stackAndHists.listOfHists, dictOfHistElementParams) :
                        if dictOfHistElementParams[histElement].draw_hist == True :
                            legend.AddEntry(hist,histElement,"l")
                    legend.SetFillStyle(0)
                    legend.Draw()

                    # boxOverZero = ROOT.TBox(0.1, 0.1, 0.8, 0.8) This is in user coordinates, need to create the object then set or draw in NDC
                    # boxOverZero.SetFillColor(ROOT.kRed)
                    # boxOverZero.SetFillStyle(1001)
                    # boxOverZero.Draw()

                    anaRegText = anaReg

                    anaRegText = renameHFSelStrings(anaRegText)
                    
                    # anaRegText = anaRegText.replace("nJets", "j")
                    # anaRegText = anaRegText.replace("nBTags", "b")
                    # anaRegText = anaRegText.replace("*", ",")
                    # anaRegText = anaRegText.replace(" &&", ",")
                    # anaRegText = anaRegText.replace("&&", ",")
                    # anaRegText = anaRegText.replace(">= ", "#geq")
                    # anaRegText = anaRegText.replace(">=", "#geq ")
                    # anaRegText = anaRegText.replace("<= ", "#leq")
                    # anaRegText = anaRegText.replace("<=", "#leq ")
                    # anaRegText = anaRegText.replace("== ", "= ")
                    # anaRegText = anaRegText.replace("==", "=")
                    # anaRegText = anaRegText.replace("(", "")
                    # anaRegText = anaRegText.replace(")", "")

                    anaRegText = anaRegText.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3j CR1(tt+light)')
                    anaRegText = anaRegText.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3j CR2(tt+#geq1b)')
                    anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '#geq4j CR1(tt+light)')
                    anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '#geq4j CR2(tt+#geq1c)')
                    anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '#geq4j SR3(tt+1b)')
                    anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '#geq4j SR2(tt+#geq1b)')
                    anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '#geq4j SR1(tt+#geq2b)')
                    
                    channelText = channel
                    ################ QUICK HACKS FOR PLOTTING WITH 2015 AND 2016 SPLITTING (AUTOMATE THIS) ###################################
                    channelText = channelText.replace("ee_2015 || ee_2016 || mumu_2015 || mumu_2016 || emu_2015 || emu_2016", "ee,mumu,emu")
                    channelText = channelText.replace("ejets_2015 || ejets_2016 || mujets_2015 || mujets_2016", "ejets,mujets")
                    ##########################################################################################################################
                    channelText = channelText.replace(" ||", ",")
                    channelText = channelText.replace("||", ",")
                    channelText = channelText.replace("(", "")
                    channelText = channelText.replace(")", "")
                    # channelText = channelText.replace("_2015", "")
                    # channelText = channelText.replace("_2016", "")
                    # channel = channelText.split(",").at(0)
                    # channelRepeatCount = re.finditer(channel, channelText)
                    # if len(channelRepeatCount) > 1 :
                    #     channelText.replace(channel, "", len(channelRepeatCount)-1)
                        

                    latexText = ROOT.TLatex()
                    decoInitXPos = 0.2
                    decoInitYPos = 0.875
                    decoLowestYPos = decoInitYPos
                    latexText.DrawLatexNDC(decoInitXPos, decoInitYPos, "#bf{#it{ATLAS}} Internal")
                    decoLowestYPos -= 0.05
                    latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), channelText)
                    decoLowestYPos -= 0.05
                    latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), anaRegText)
                    decoLowestYPos -= 0.05
                    if printFileName :
                        latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), dictOfHistElementParams.values()[0].sample)
                        decoLowestYPos -= 0.05
                    #decoLowestYPos -= 0.05 # One more notch on LowestYPos as DrawLatexNDC y pos
                                           # is top of latex text.
                                           # I should work out the height of the last decoration
                                           # and use this instead to make this dynamic

                    # Finding decoration height in user co-ords, and adding this much space
                    # to the hist y-range
                    decoHeightNDC   = 1.0 - (histPad.GetTopMargin() + decoLowestYPos)
                    legendHeightNDC = legend.GetY2() - legend.GetY1()
                    histHeightNDC   = 1.0 - (histPad.GetTopMargin() + histPad.GetBottomMargin())
                    histMax = stackAndHists.stack.GetMaximum('nostack') # This gives the max of the histogram itself, not the max of the axes, meaning adding height is off
                    decoHeightUser   = (decoHeightNDC / histHeightNDC) * histMax
                    legendHeightUser = (legendHeightNDC / histHeightNDC) * histMax
                    #stackAndHists.stack.SetMaximum(1.1*histMax+decoHeightUser) # I think this factor of 1.1 is arbitrary and non-general
                    stackAndHists.stack.SetMaximum(1.3*histMax+legendHeightUser)

                    ratioPad.cd()
                    ratioStackAndHists.stack.Draw('nostack E1')
                    ratioStackAndHists.stack.GetXaxis().SetTitle(params.xtitle)
                    ratioStackAndHists.stack.GetYaxis().SetTitle('#frac{MC}{'+ratioDenom+'}')
                    ratioStackAndHists.stack.GetXaxis().SetTitleSize(0.1)
                    ratioStackAndHists.stack.GetYaxis().SetTitleSize(0.08)
                    ratioStackAndHists.stack.GetXaxis().SetTitleOffset(1.)
                    ratioStackAndHists.stack.GetYaxis().SetTitleOffset(0.8)
                    ratioStackAndHists.stack.GetXaxis().SetLabelSize(0.1)
                    ratioStackAndHists.stack.GetYaxis().SetLabelSize(0.1)
                    ratioStackAndHists.stack.GetYaxis().SetNdivisions(5, 3, 0)
                    ratioStackAndHists.stack.SetMinimum(params.ratio_y_lo)
                    ratioStackAndHists.stack.SetMaximum(params.ratio_y_hi)

                    ratioOneLine = ROOT.TLine()
                    ratioOneLine.SetLineColor(15)
                    ratioOneLine.SetLineStyle(7)
                    ratioOneLine.SetLineWidth(3)
                    ratioOneLine.DrawLine(ratioStackAndHists.stack.GetXaxis().GetXmin(), 1., ratioStackAndHists.stack.GetXaxis().GetXmax(), 1.)

                    saveFolder = channel+'_'+anaReg
                    saveFolder = renameHFSelStrings(saveFolder)
                    saveFolder = saveFolder.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3jCR1')
                    saveFolder = saveFolder.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3jCR2')
                    saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '4jCR1')
                    saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '4jCR2')
                    saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '4jSR3')
                    saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '4jSR2')
                    saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '4jSR1')
                    saveFolder = saveFolder.replace("+", "")
                    saveFolder = saveFolder.replace("*", "")
                    saveFolder = saveFolder.replace(" ", "")
                    saveFolder = saveFolder.replace(",", "")
                    saveFolder = saveFolder.replace("(", "")
                    saveFolder = saveFolder.replace(")", "")
                    saveFolder = saveFolder.replace("==", "E")
                    saveFolder = saveFolder.replace("=", "E")
                    saveFolder = saveFolder.replace("&", "")
                    saveFolder = saveFolder.replace("|", "")
                    saveFolder = saveFolder.replace("#", "")
                    saveFolder = saveFolder.replace(">", "GT")
                    saveFolder = saveFolder.replace("<", "LT")
                    ROOT.gSystem.mkdir(outDir)
                    ROOT.gSystem.mkdir(outDir+'/'+saveFolder)

                    canvas.SaveAs(outDir+'/'+saveFolder+'/'+params.name+'.png')#canvas.SaveAs(outDir+'/'+saveFolder+'/'+params.variable+'.png')
                    canvas.SaveAs(outDir+'/'+saveFolder+'/'+params.name+'.pdf')#canvas.SaveAs(outDir+'/'+saveFolder+'/'+params.variable+'.pdf')
                    
                    histPad.Clear()
                    ratioPad.Clear()

            canvas.Close()
            for stackWithHists in listOfStackWithHists :
                for hist in stackWithHists.listOfHists :
                    hist.Delete()
            for stackWithHists in listOfRatioStackWithHists :
                for hist in stackWithHists.listOfHists :
                    hist.Delete()
            tempStack.Delete()
            tempRatioStack.Delete()

    # CK TEMP: quick for q/g

################### tables #######################################
    gOvergPluslFile.write('\\begin{tabular}{lccccccc}\n\hline \n')
    gOvergPluslFile.write('sample')
    for anaReg in listOfAnaRegs :
        formatAnaReg = anaReg
        # formatAnaReg = formatAnaReg.replace(' == ', '')
        # formatAnaReg = formatAnaReg.replace(' >= ', '$\geq$')
        # formatAnaReg = formatAnaReg.replace(')', 'b')
        # formatAnaReg = formatAnaReg.replace('(nJets', '')
        # formatAnaReg = formatAnaReg.replace('nBTags', '')
        # formatAnaReg = formatAnaReg.replace(' && ', 'j')
      
        formatAnaReg = formatAnaReg.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3j CR1(tt+light)')
        formatAnaReg = formatAnaReg.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3j CR1(tt+\geq1b)')
        formatAnaReg = formatAnaReg.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '\ge4j CR1(tt+light)')
        formatAnaReg = formatAnaReg.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '\ge4j CR2(tt+\geq1c)')
        formatAnaReg = formatAnaReg.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '\ge4j SR3(tt+1b)')
        formatAnaReg = formatAnaReg.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '\ge4j SR2(tt+\geq1b)')
        formatAnaReg = formatAnaReg.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '\ge4j SR1(tt+\geq2b)')
      
        gOvergPluslFile.write(' & '+formatAnaReg)
    gOvergPluslFile.write('\\\\ \n\hline \n')
    for histElement in dictOfHistElementParams :
        formatHistElement = histElement
        formatHistElement = formatHistElement.replace('#', '\\')
        formatHistElement = '$'+formatHistElement+'$'
        gOvergPluslFile.write(formatHistElement)
        for anaReg in listOfAnaRegs :
            gOvergPluslFile.write(' & '+gOvergPluslDict[histElement][anaReg])
        gOvergPluslFile.write('\\\\ \n')
    gOvergPluslFile.write('\hline \n\end{tabular}')
    gOvergPluslFile.close()
#################################################################

################### plots #######################################    
    gOvergPlusl_plots = {}
    for histElement in dictOfHistElementParams :
        gOvergPlusl_plots[histElement] = {}
        for anaReg in listOfAnaRegs :
            values = []
            errors = []
            for nJet_valPair in gOvergPlusl_nJets_Dict[histElement][anaReg] :
                values.append(nJet_valPair[0])
                errors.append(nJet_valPair[1])
                nJets        = (3,4,5,6,7,8,9,10)
                nJets_errors = (0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5)
            gOvergPlusl_plots[histElement][anaReg] = ROOT.TGraphErrors(len(gOvergPlusl_nJets_Dict[histElement][anaReg]), (ctypes.c_float * len(nJets))(*nJets), (ctypes.c_float * len(values))(*values), (ctypes.c_float * len(nJets_errors))(*nJets_errors), (ctypes.c_float * len(errors))(*errors))

            gOvergPlusl_plots[histElement][anaReg].SetName('h_'+histElement)
            gOvergPlusl_plots[histElement][anaReg].SetTitle(histElement)
            gOvergPlusl_plots[histElement][anaReg].SetLineColor(dictOfHistElementParams[histElement].line_color)
            gOvergPlusl_plots[histElement][anaReg].SetMarkerColor(dictOfHistElementParams[histElement].line_color)
            gOvergPlusl_plots[histElement][anaReg].SetFillColor(dictOfHistElementParams[histElement].fill_color)
            gOvergPlusl_plots[histElement][anaReg].SetFillStyle(dictOfHistElementParams[histElement].fill_style)

    canvas2 = ROOT.TCanvas('canvas', 'g/(g+l)', 1000, 1000)
    canvas2.cd()
    for channel in listOfChannels :
        for anaReg in listOfAnaRegs :
           graphStack = ROOT.TMultiGraph("g_gOvergPlusl", "g/(g+l)")
           graphStack.SetTitle("g/(g+l);nJets;#frac{g}{g+l}");
           for histElement in dictOfHistElementParams :
               graphStack.Add(gOvergPlusl_plots[histElement][anaReg])

           graphStack.Draw("AP")
       
           saveFolder = channel+'_'+anaReg
           saveFolder = renameHFSelStrings(saveFolder)
           saveFolder = saveFolder.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3jCR1')
           saveFolder = saveFolder.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3jCR2')
           saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '4jCR1')
           saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '4jCR2')
           saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '4jSR3')
           saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '4jSR2')
           saveFolder = saveFolder.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '4jSR1')
           saveFolder = saveFolder.replace("+", "")
           saveFolder = saveFolder.replace("*", "")
           saveFolder = saveFolder.replace(" ", "")
           saveFolder = saveFolder.replace(",", "")
           saveFolder = saveFolder.replace("(", "")
           saveFolder = saveFolder.replace(")", "")
           saveFolder = saveFolder.replace("==", "E")
           saveFolder = saveFolder.replace("=", "E")
           saveFolder = saveFolder.replace("&", "")
           saveFolder = saveFolder.replace("|", "")
           saveFolder = saveFolder.replace("#", "")
           saveFolder = saveFolder.replace(">", "GT")
           saveFolder = saveFolder.replace("<", "LT")
           ROOT.gSystem.mkdir(outDir)
           ROOT.gSystem.mkdir(outDir+'/'+saveFolder)

           leg = canvas2.BuildLegend(0.6,0.65,0.95,0.949, "")#, "LP")
           leg.SetFillStyle(0)

           anaRegText = anaReg

           anaRegText = renameHFSelStrings(anaRegText)
                    
           # anaRegText = anaRegText.replace("nJets", "j")
           # anaRegText = anaRegText.replace("nBTags", "b")
           # anaRegText = anaRegText.replace("*", ",")
           # anaRegText = anaRegText.replace(" &&", ",")
           # anaRegText = anaRegText.replace("&&", ",")
           # anaRegText = anaRegText.replace(">= ", "#geq")
           # anaRegText = anaRegText.replace(">=", "#geq ")
           # anaRegText = anaRegText.replace("<= ", "#leq")
           # anaRegText = anaRegText.replace("<=", "#leq ")
           # anaRegText = anaRegText.replace("== ", "= ")
           # anaRegText = anaRegText.replace("==", "=")
           # anaRegText = anaRegText.replace("(", "")
           # anaRegText = anaRegText.replace(")", "")

           anaRegText = anaRegText.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3j CR1(tt+light)')
           anaRegText = anaRegText.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3j CR2(tt+#geq1b)')
           anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '#geq4j CR1(tt+light)')
           anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '#geq4j CR2(tt+#geq1c)')
           anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '#geq4j SR3(tt+1b)')
           anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '#geq4j SR2(tt+#geq1b)')
           anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '#geq4j SR1(tt+#geq2b)')
           
           channelText = channel
           ################ QUICK HACKS FOR PLOTTING WITH 2015 AND 2016 SPLITTING (AUTOMATE THIS) ###################################
           channelText = channelText.replace("ee_2015 || ee_2016 || mumu_2015 || mumu_2016 || emu_2015 || emu_2016", "ee,mumu,emu")
           channelText = channelText.replace("ejets_2015 || ejets_2016 || mujets_2015 || mujets_2016", "ejets,mujets")
           ##########################################################################################################################
           channelText = channelText.replace(" ||", ",")
           channelText = channelText.replace("||", ",")
           channelText = channelText.replace("(", "")
           channelText = channelText.replace(")", "")
           # channelText = channelText.replace("_2015", "")
           # channelText = channelText.replace("_2016", "")
           # channel = channelText.split(",").at(0)
           # channelRepeatCount = re.finditer(channel, channelText)
           # if len(channelRepeatCount) > 1 :
           #     channelText.replace(channel, "", len(channelRepeatCount)-1)

           latexText = ROOT.TLatex()
           decoInitXPos = 0.2
           decoInitYPos = 0.875
           decoLowestYPos = decoInitYPos
           latexText.DrawLatexNDC(decoInitXPos, decoInitYPos, "#bf{#it{ATLAS}} Internal")
           decoLowestYPos -= 0.05
           latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), channelText)
           decoLowestYPos -= 0.05
           latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), anaRegText)
           decoLowestYPos -= 0.05

           legendHeightNDC = 0.949 - 0.65
           graphHeightNDC   = 1.0 - (canvas2.GetTopMargin() + canvas2.GetBottomMargin())
           graphMax = graphStack.GetYaxis().GetXmax()
           legendHeightUser = (legendHeightNDC / graphHeightNDC) * graphMax
           graphStack.SetMaximum(1.3*graphMax+legendHeightUser)
       
           canvas2.SaveAs(outDir+'/'+saveFolder+'/gOvergPlusl.png')
           canvas2.SaveAs(outDir+'/'+saveFolder+'/gOvergPlusl.pdf')
                       
           canvas2.Clear()



    # CK test

    # gOvergPluslDict_vals = {'t#bar{t}H':{'(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))':0.1, '(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)':0.2, '(nJets >= 4 && DilepRegion_A3 == 5)':0.3, '(nJets >= 4 && DilepRegion_A3 == 4)':0.4, '(nJets >= 4 && DilepRegion_A3 == 2)':0.5, '(nJets >= 4 && DilepRegion_A3 == 3)':0.6, '(nJets >= 4 && DilepRegion_A3 == 1)':0.7,}, 't#bar{t}+light':{'(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))':0.01, '(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)':0.02, '(nJets >= 4 && DilepRegion_A3 == 5)':0.03, '(nJets >= 4 && DilepRegion_A3 == 4)':0.04, '(nJets >= 4 && DilepRegion_A3 == 2)':0.05, '(nJets >= 4 && DilepRegion_A3 == 3)':0.06, '(nJets >= 4 && DilepRegion_A3 == 1)':0.07,}}
    # gOvergPluslDict_errs = {'t#bar{t}H':{'(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))':0.1, '(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)':0.2, '(nJets >= 4 && DilepRegion_A3 == 5)':0.3, '(nJets >= 4 && DilepRegion_A3 == 4)':0.4, '(nJets >= 4 && DilepRegion_A3 == 2)':0.5, '(nJets >= 4 && DilepRegion_A3 == 3)':0.6, '(nJets >= 4 && DilepRegion_A3 == 1)':0.7,}, 't#bar{t}+light':{'(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))':0.01, '(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)':0.02, '(nJets >= 4 && DilepRegion_A3 == 5)':0.03, '(nJets >= 4 && DilepRegion_A3 == 4)':0.04, '(nJets >= 4 && DilepRegion_A3 == 2)':0.05, '(nJets >= 4 && DilepRegion_A3 == 3)':0.06, '(nJets >= 4 && DilepRegion_A3 == 1)':0.07,}}

    # region comparison plot
    for channel in listOfChannels :
        regionStack_temp = ROOT.THStack('hstack_regPlot', '#frac{g}{g+l}')
        for histElement in dictOfHistElementParams :
            regionPlot_temp = ROOT.TH1D('h_regPlot_'+histElement, histElement, 7, 0, 7) # 7 regions
            for anaRegItr in enumerate(listOfAnaRegs) :
                regionPlot_temp.SetBinContent(anaRegItr[0]+1, gOvergPluslDict_vals[histElement][anaRegItr[1]])
                regionPlot_temp.SetBinError(anaRegItr[0]+1, gOvergPluslDict_errs[histElement][anaRegItr[1]])

            regionPlot_temp.SetLineColor(dictOfHistElementParams[histElement].line_color)
            regionPlot_temp.SetLineStyle(dictOfHistElementParams[histElement].line_style)
            #regionPlot_temp.SetLineWidth(3)
            #regionPlot_temp.SetMarkerStyle(1)
            regionPlot_temp.SetMarkerColor(dictOfHistElementParams[histElement].line_color)
            regionPlot_temp.SetFillColor(dictOfHistElementParams[histElement].fill_color)
            regionPlot_temp.SetFillStyle(dictOfHistElementParams[histElement].fill_style)
            
            regionStack_temp.Add(regionPlot_temp)

        regionStack_temp.Draw('nostack E')
        regionStack_temp.GetYaxis().SetTitle('#frac{g}{g+l}')
        regionStack_temp.GetXaxis().CenterLabels()
        for anaRegItr in enumerate(listOfAnaRegs) :
            anaRegText = anaRegItr[1]

            anaRegText = renameHFSelStrings(anaRegText)
            
            anaRegText = anaRegText.replace('(nJets == 3 && nBTags_77 >= 2 && !(nBTags_70 >= 3&& nBTags_60 >= 1))', '3j CR1')
            anaRegText = anaRegText.replace('(nJets == 3 && nBTags_70 >= 3 && nBTags_60 >= 1)', '3j CR2')
            anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 5)', '4j CR1')
            anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 4)', '4j CR2')
            anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 2)', '4j SR3')
            anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 3)', '4j SR2')
            anaRegText = anaRegText.replace('(nJets >= 4 && DilepRegion_A3 == 1)', '4j SR1')

            regionStack_temp.GetXaxis().SetBinLabel(anaRegItr[0]+1, anaRegText)

        leg = canvas2.BuildLegend(0.6,0.65,0.95,0.949, "")#, "LP")
        leg.SetFillStyle(0)
            
        channelText = channel
        ################ QUICK HACKS FOR PLOTTING WITH 2015 AND 2016 SPLITTING (AUTOMATE THIS) ###################################
        channelText = channelText.replace("ee_2015 || ee_2016 || mumu_2015 || mumu_2016 || emu_2015 || emu_2016", "ee,mumu,emu")
        channelText = channelText.replace("ejets_2015 || ejets_2016 || mujets_2015 || mujets_2016", "ejets,mujets")
        ##########################################################################################################################
        channelText = channelText.replace(" ||", ",")
        channelText = channelText.replace("||", ",")
        channelText = channelText.replace("(", "")
        channelText = channelText.replace(")", "")
            
            
        latexText = ROOT.TLatex()
        decoInitXPos = 0.2
        decoInitYPos = 0.875
        decoLowestYPos = decoInitYPos
        latexText.DrawLatexNDC(decoInitXPos, decoInitYPos, "#bf{#it{ATLAS}} Internal")
        decoLowestYPos -= 0.05
        latexText.DrawLatexNDC(decoInitXPos, (decoLowestYPos), channelText)
        decoLowestYPos -= 0.05

        legendHeightNDC = 0.949 - 0.65
        regPlotHeightNDC   = 1.0 - (canvas2.GetTopMargin() + canvas2.GetBottomMargin())
        regPlotMax = regionStack_temp.GetYaxis().GetXmax()
        legendHeightUser = (legendHeightNDC / regPlotHeightNDC) * regPlotMax
        regionStack_temp.SetMaximum(1.3*regPlotMax+legendHeightUser)

        canvas2.SaveAs(outDir+'/gOvergPlusl_regs.png')
        canvas2.SaveAs(outDir+'/gOvergPlusl_regs.pdf')
        
        canvas2.Clear()

    canvas2.Close()

#################################################################
            
    print "Done"
